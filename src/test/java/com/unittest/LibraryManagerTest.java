package com.unittest;

import com.libararymanagmentsystem.bookstore.Book;
import com.libararymanagmentsystem.librarymanager.LibraryManager;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

public class LibraryManagerTest {

    private LibraryManager libraryManager;

    @BeforeClass
    public void setUp() {
        libraryManager = new LibraryManager();
        libraryManager.addBookToLibrary("Java: A Beginner's Guide", "Herbert Schildt", 2019);
        libraryManager.addBookToLibrary("Effective Java", "Joshua Bloch", 2018);
        libraryManager.addBookToLibrary("Best practices", "Joshua Bloch", 2019);
        libraryManager.addBookToLibrary("Clean Code", "Robert C. Martin", 2008);
    }

    @Test
    public void testSearchBooksByTitle() {
        String searchTitle = "Java: A Beginner's Guide";
        List<Book> booksByTitle = libraryManager.searchBooksByTitle(searchTitle);

        Assert.assertEquals(1, booksByTitle.size());
        Book book = booksByTitle.get(0);
        Assert.assertEquals(searchTitle, book.getTitle());
    }

    @Test
    public void testSearchBooksByAuthor() {
        String searchAuthor = "Joshua Bloch";
        List<Book> booksByAuthor = libraryManager.searchBooksByAuthor(searchAuthor);

        Assert.assertEquals(2, booksByAuthor.size());
        Book book = booksByAuthor.get(0);
        Assert.assertEquals(searchAuthor, book.getAuthor());
    }

    @Test
    public void testSearchBooksByYear() {
        int searchYear = 2019;
        List<Book> booksByAuthor = libraryManager.searchBooksByYear(searchYear);

        Assert.assertEquals(2, booksByAuthor.size());
        Book book = booksByAuthor.get(0);
        Assert.assertEquals(searchYear, book.getYear());
    }

    @Test
    public void testGetAllBooksInLibrary() {
        List<Book> allBooks = libraryManager.getAllBooksInLibrary();

        Assert.assertEquals(4, allBooks.size());
    }
}
