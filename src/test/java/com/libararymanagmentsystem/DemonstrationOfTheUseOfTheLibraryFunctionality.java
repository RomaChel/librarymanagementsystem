package com.libararymanagmentsystem;

import com.libararymanagmentsystem.librarymanager.LibraryManager;

import static com.libararymanagmentsystem.print.PrintAllBooks.printAllBooks;
import static com.libararymanagmentsystem.search.SearchBy.*;

public class DemonstrationOfTheUseOfTheLibraryFunctionality {

    public static void main(String[] args) {
        LibraryManager libraryManager = new LibraryManager();

        // Adding a book to the library
        libraryManager.addBookToLibrary("Java: A Beginner's Guide", "Herbert Schildt", 2019);
        libraryManager.addBookToLibrary("Effective Java", "Joshua Bloch", 2018);
        libraryManager.addBookToLibrary("Clean Code", "Robert C. Martin", 2008);

        // Search for a book by title
        String searchTitle = "Java: A Beginner's Guide";
        searchByTitle(libraryManager, searchTitle);

        // Search for a book by author
        String searchAuthor = "Joshua Bloch";
        searchByAuthor(libraryManager, searchAuthor);

        int searchYear = 2008;
        searchByYear(libraryManager, searchYear);

        // Display a list of all books in the library
        printAllBooks(libraryManager);
    }
}
