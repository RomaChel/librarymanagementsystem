package com.libararymanagmentsystem.search;

import com.libararymanagmentsystem.bookstore.Book;
import com.libararymanagmentsystem.librarymanager.LibraryManager;

import java.util.List;

public class SearchBy {

    public static void searchByTitle(LibraryManager libraryManager, String title) {
        List<Book> booksByTitle = libraryManager.searchBooksByTitle(title);
        System.out.println("Books with title '" + title + "':");
        for (Book book : booksByTitle) {
            System.out.println(book.getTitle() + " by " + book.getAuthor() + " (" + book.getYear() + ")");
        }
        System.out.println();
    }

    public static void searchByAuthor(LibraryManager libraryManager, String author) {
        List<Book> booksByAuthor = libraryManager.searchBooksByAuthor(author);
        System.out.println("Books by author '" + author + "':");
        for (Book book : booksByAuthor) {
            System.out.println(book.getTitle() + " by " + book.getAuthor() + " (" + book.getYear() + ")");
        }
        System.out.println();
    }

    public static void searchByYear(LibraryManager libraryManager, int year) {
        List<Book> booksByAuthor = libraryManager.searchBooksByYear(year);
        System.out.println("Books by year '" + year + "':");
        for (Book book : booksByAuthor) {
            System.out.println(book.getTitle() + " by " + book.getAuthor() + " (" + book.getYear() + ")");
        }
        System.out.println();
    }

}
