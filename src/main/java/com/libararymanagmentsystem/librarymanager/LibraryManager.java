package com.libararymanagmentsystem.librarymanager;

import com.libararymanagmentsystem.bookstore.Book;
import com.libararymanagmentsystem.library.Library;

import java.util.List;

public class LibraryManager {

    private Library library;

    public LibraryManager() {
        library = new Library();
    }

    public void addBookToLibrary(String title, String author, int year) {
        Book book = new Book(title, author, year);
        library.addBook(book);
    }

    public List<Book> searchBooksByTitle(String title) {
        return library.searchByTitle(title);
    }

    public List<Book> searchBooksByAuthor(String author) {
        return library.searchByAuthor(author);
    }

    public List<Book> searchBooksByYear(int year) {
        return library.searchByYear(year);
    }

    public List<Book> getAllBooksInLibrary() {
        return library.getAllBooks();
    }
}

