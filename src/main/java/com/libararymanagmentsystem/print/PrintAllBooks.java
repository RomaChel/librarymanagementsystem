package com.libararymanagmentsystem.print;

import com.libararymanagmentsystem.bookstore.Book;
import com.libararymanagmentsystem.librarymanager.LibraryManager;

import java.util.List;

public class PrintAllBooks {

    public static void printAllBooks(LibraryManager libraryManager) {
        List<Book> allBooks = libraryManager.getAllBooksInLibrary();
        System.out.println("All books in the library:");
        for (Book book : allBooks) {
            System.out.println(book.getTitle() + " by " + book.getAuthor() + " (" + book.getYear() + ")");
        }
    }
}
